import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class FlickrService {
    result$: Observable<any>;
    key = '2132616388be89c989e36da3ab2a28c3';
    constructor(private _http: Http) { };

    getResult(query: string, page : number) {
        let url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${this.key}&tags=${query}&per_page=20&format=json&page=${page}&nojsoncallback=1`;
        return this._http
            .get(url)
            .map(res => res.json())
            .map((val) => {
                if (val.stat === 'ok') {
                   return val.photos.photo.map((photo: any) => {
                         return {
                             url: `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_m.jpg`,
                             title: photo.title
                         }
                    })
                }
                else {
                    return [];
                }
            });
    }
}
