import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';
import { FlickrService } from '../../services/flickr.service';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  constructor(private _flickrService: FlickrService,public elemRef:ElementRef) { }
  @Input() photo;
  photos:any[] = [];
  @Input() query;
  page:number = 0;
  ngOnInit() {
  }

  // @HostListener("window:scroll", [])
  @HostListener("window:scroll", [])
  onWindowScroll() {
    const componentPosition = this.elemRef.nativeElement.offsetTop;
    const scrollPosition = this.elemRef.nativeElement.pageYOffset;
    // const scrollPosition = window.pageYOffset;
    console.log(`This is componentPosition ${componentPosition} and this is scrollPosition ${scrollPosition}`);
    if (scrollPosition >= componentPosition) {
      this.fetchData();
    }
  }
  fetchData() {
    // console.log("This is ################### Photos:: ",this.photo);
    this.page++;
    this._flickrService.getResult(this.query, this.page)
      .debounceTime(500)
      .subscribe(value => {
        value.map(val => this.photos.push(val));
        // console.log("This is photos: ", this.photos.length);
        return this.photos;
      });
  }

}
