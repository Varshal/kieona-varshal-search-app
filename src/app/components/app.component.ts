import { Component, OnInit, HostListener, ElementRef, Input } from '@angular/core';
import { FlickrService } from '../services/flickr.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {CookieService} from 'angular2-cookie/core';


@Component({
  //Angular-cli doesn't like moduleId'
  //moduleId: module.id,
  selector: 'app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  providers: [FlickrService, CookieService]
})
export class MainAppComponent implements OnInit {
  searchControl = new FormControl();

  page: number = 0;
  model$: Observable<any>;
  query: string;
  photos: any[] = [];
  searchHistory : string = "";
  constructor(private _formBuilder: FormBuilder, private _flickrService: FlickrService, public elemRef:ElementRef, private _cookieService:CookieService) {
  }
  ngOnInit() {
    this.searchHistory = this.getCookie('query');
    this.fetchSearchData();
  }

  getCookie(key: string){
    return this._cookieService.get(key);
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.fetchData();
    }
  }

  fetchSearchData() {
    this.searchControl.valueChanges
      .switchMap((query: string) => {
        this.query = query;
        this.photos=[];
        return this._flickrService.getResult(query, this.page)
      }
      )
      .subscribe(value => {
        if(this.query != "") {
          if(this.searchHistory == undefined) {
            this.searchHistory = "";
          }
          this.searchHistory += this.query + ",";
          this._cookieService.putObject('query', this.searchHistory);
        }
        value.map(val => this.photos.push(val));
        return this.photos;
      });
  }

  fetchData() {
    this.page++;
    this._flickrService.getResult(this.query, this.page)
      .debounceTime(500)
      .subscribe(value => {
        value.map(val => this.photos.push(val));
        return this.photos;
      });
  }
}
